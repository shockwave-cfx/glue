local INPUT_VEH_DUCK = 73

Citizen.CreateThread(function ()
  local function GlueLoop()
    -- AttachEntityToEntity
    -- GetEntityAttachedTo
    -- IsEntityAttached
    -- ProcessEntityAttachments
    -- DetachEntity

    if not IsControlJustPressed(0, INPUT_VEH_DUCK) then
      return
    end

    local player = PlayerPedId()

    if not IsEntityGlued(player) then
      local vehicle = GetVehiclePedStandsOn(player)

      if vehicle == 0 then
        return
      end

      GlueEntities(player, vehicle)
    else
      UnglueEntity(player)
    end
  end

  while true do
    Citizen.Wait(0)
    GlueLoop()
  end
end)
